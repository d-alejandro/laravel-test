<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HolidayFormRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'date' => 'required|date_format:"d.m.Y"',
        ];
    }

    public function messages(): array
    {
        return [
            'date.date_format' => 'The :attribute does not match the format day.month.year',
        ];
    }
}
