<?php

namespace App\Http\Services;

use App\Http\Helpers\HolidayHelper;
use App\Http\Models\Holiday;
use App\Http\Repositories\HolidayRepository;

class HolidayService
{
    private $repository;

    public function __construct(HolidayRepository $repository)
    {
        $this->repository = $repository;
    }

    public function search($presetDate): array
    {
        $presetYear = HolidayHelper::getYear($presetDate);

        $allHolidays = [];

        foreach (Holiday::all() as $holiday) {
            if ($this->isHolidayByDate($holiday)) {
                $allHolidays = array_merge($allHolidays, $this->repository->searchHolidayByDate($holiday, $presetYear));
            } elseif ($this->isHolidayByWeek($holiday)) {
                $allHolidays = array_merge($allHolidays, $this->repository->searchHolidayByWeek($holiday, $presetYear));
            } elseif ($this->isHolidayByInterval($holiday)) {
                $allHolidays = array_merge(
                    $allHolidays,
                    $this->repository->searchHolidayByInterval($holiday, $presetYear)
                );
            }
        }

        foreach ($allHolidays as $key => $val) {
            if ($val['month-day'] !== date('Y-m-d', strtotime($presetDate))) {
                unset($allHolidays[$key]);
            }
        }

        return $allHolidays;
    }

    private function isHolidayByDate(Holiday $holiday): bool
    {
        if ($holiday->firstDate !== null && $holiday->lastDate === null && $holiday->weekNumber === null) {
            return true;
        }
        return false;
    }

    private function isHolidayByWeek(Holiday $holiday): bool
    {
        if ($holiday->firstDate !== null &&
            $holiday->lastDate === null &&
            $holiday->weekNumber !== null &&
            $holiday->dayName !== null) {
            return true;
        }
        return false;
    }

    private function isHolidayByInterval(Holiday $holiday): bool
    {
        if ($holiday->firstDate !== null &&
            $holiday->lastDate !== null &&
            $holiday->weekNumber === null &&
            $holiday->dayName === null) {
            return true;
        }
        return false;
    }
}
