<?php

namespace App\Http\Controllers;

use App\Http\Requests\HolidayFormRequest;
use App\Http\Services\HolidayService;

class HomeController extends Controller
{
    private $service;

    public function __construct(HolidayService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return view('welcome');
    }

    public function isHoliday(HolidayFormRequest $formRequest)
    {
        try {
            $messages = $this->service->search($formRequest['date']);
        } catch (\Exception $e) {
            return redirect()->route('home')->with(
                [
                    'date' => $formRequest['date'],
                    'error' => $e->getMessage(),
                ]
            );
        }

        return redirect()->route('home')->with(
            [
                'date' => $formRequest['date'],
                'messages' => $messages,
            ]
        );
    }
}
