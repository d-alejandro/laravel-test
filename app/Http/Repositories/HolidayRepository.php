<?php

namespace App\Http\Repositories;

use App\Http\Helpers\HolidayHelper;
use App\Http\Models\DayName;
use App\Http\Models\Holiday;
use App\Http\Models\Weeks;

class HolidayRepository
{
    public function searchHolidayByInterval(Holiday $holiday, $presetYear): array
    {
        $countDays = HolidayHelper::getCountDaysBetweenDates($holiday, $presetYear);
        $month = HolidayHelper::getMonthNumber($holiday->firstDate);
        $dayOfMonth = HolidayHelper::getDayOfMonth($holiday->firstDate);

        $holidaysArray = [];

        for ($day = 0; $day < $countDays; $day++) {
            $holidaysArray[] = HolidayHelper::generateDateHoliday(
                $holiday->name,
                $month,
                $dayOfMonth + $day,
                $presetYear
            );
        }

        return $holidaysArray;
    }

    public function searchHolidayByWeek(Holiday $holiday, $presetYear): array
    {
        $monthOfHoliday = HolidayHelper::getMonthNumber($holiday->firstDate);

        $holidaysArray = [];

        $lastDay = HolidayHelper::getDaysCount($monthOfHoliday, 1, $presetYear);
        $lastWeek = HolidayHelper::getWeekOfMonth($monthOfHoliday, $lastDay, $presetYear);

        if ($holiday->weekNumber == Weeks::WEEK_LAST) {
            $daySearch = $lastDay;
            $currentWeekNumber = $lastWeek;
            $currentDayName = HolidayHelper::getDayName($monthOfHoliday, $lastDay, $presetYear);

            while ($lastWeek == $currentWeekNumber && $holiday->dayName != $currentDayName) {
                $daySearch--;
                $currentWeekNumber = HolidayHelper::getWeekOfMonth($monthOfHoliday, $daySearch, $presetYear);
                $currentDayName = HolidayHelper::getDayName($monthOfHoliday, $daySearch, $presetYear);
            }
        } else {
            $daysCount = HolidayHelper::getDaysCount($monthOfHoliday, 1, $presetYear);
            $currentWeekNumber = HolidayHelper::getWeekOfMonth($monthOfHoliday, 1, $presetYear);
            $currentDayName = HolidayHelper::getDayName($monthOfHoliday, 1, $presetYear);

            for ($daySearch = 1; $daySearch < $daysCount; $daySearch++) {
                $currentWeekNumber = HolidayHelper::getWeekOfMonth($monthOfHoliday, $daySearch, $presetYear);
                $currentDayName = HolidayHelper::getDayName($monthOfHoliday, $daySearch, $presetYear);

                if ($this->isFound($holiday->weekNumber, $holiday->dayName, $currentWeekNumber, $currentDayName)) {
                    break;
                }
            }
        }

        $weekNumber = $holiday->weekNumber == Weeks::WEEK_LAST ? $lastWeek : $holiday->weekNumber;

        if ($this->isFound($weekNumber, $holiday->dayName, $currentWeekNumber, $currentDayName)) {
            $holidaysArray[] = HolidayHelper::generateDateHoliday(
                $holiday->name,
                $monthOfHoliday,
                $daySearch,
                $presetYear
            );

            $array = $this->searchHolidayOnWeekend($holiday->name, $monthOfHoliday, $daySearch, $presetYear);

            if ($array) {
                $holidaysArray[] = $array;
            }
        }

        return $holidaysArray;
    }

    public function searchHolidayByDate(Holiday $holiday, $presetYear): array
    {
        $monthNumber = HolidayHelper::getMonthNumber($holiday->firstDate);
        $dayOfMonth = HolidayHelper::getDayOfMonth($holiday->firstDate);

        $holidaysArray[] = HolidayHelper::generateDateHoliday($holiday->name, $monthNumber, $dayOfMonth, $presetYear);

        $array = $this->searchHolidayOnWeekend($holiday->name, $monthNumber, $dayOfMonth, $presetYear);

        if ($array) {
            $holidaysArray[] = $array;
        }

        return $holidaysArray;
    }

    private function searchHolidayOnWeekend(string $holidayName, int $month, int $day, int $year): array
    {
        $holidaysArray = [];

        $weekendNumber = array_search(HolidayHelper::getDayName($month, $day, $year), DayName::weekendDays());

        if ($weekendNumber !== false) {
            $holidaysArray = HolidayHelper::generateDateHoliday($holidayName, $month, $day + $weekendNumber + 1, $year);
        }

        return $holidaysArray;
    }

    private function isFound($weekNumber, $dayName, $currentWeekNumber, $currentDayName): bool
    {
        if ($weekNumber == $currentWeekNumber && $dayName == $currentDayName) {
            return true;
        }
        return false;
    }
}
