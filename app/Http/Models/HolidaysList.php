<?php

namespace App\Http\Models;

use App\Http\Models\HolidayStruct as Holiday;

class HolidaysList
{
    /**
     * @return Holiday[]
     */
    public static function getList(): array
    {
        return [
            new Holiday(
                '1st of January',
                '2000-01-01',
                null,
                null,
                null
            ),
            new Holiday(
                '7th of January',
                '2000-01-07',
                null,
                null,
                null
            ),
            new Holiday(
                'From 1st of May till 7th of May',
                '2000-05-01',
                '2000-05-07',
                null,
                null
            ),
            new Holiday(
                'Monday of the 3rd week of January',
                '2000-01-01',
                null,
                Weeks::WEEK_THIRD,
                DayName::DAY_NAME_MONDAY
            ),
            new Holiday(
                'Monday of the last week of March',
                '2000-03-01',
                null,
                Weeks::WEEK_LAST,
                DayName::DAY_NAME_MONDAY
            ),
            new Holiday(
                'Thursday of the 4th week of November',
                '2000-11-01',
                null,
                Weeks::WEEK_FOURTH,
                DayName::DAY_NAME_THURSDAY
            ),
        ];
    }
}
