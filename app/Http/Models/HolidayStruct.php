<?php

namespace App\Http\Models;

class HolidayStruct
{
    public $name;
    public $firstDate;
    public $lastDate;
    public $weekNumber;
    public $dayName;

    public function __construct($name, $firstDate, $lastDate, $weekNumber, $dayName)
    {
        $this->name = $name;
        $this->firstDate = $firstDate;
        $this->lastDate = $lastDate;
        $this->weekNumber = $weekNumber;
        $this->dayName = $dayName;
    }
}
