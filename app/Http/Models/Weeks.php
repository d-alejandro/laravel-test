<?php

namespace App\Http\Models;

class Weeks
{
    const WEEK_FIRST = '1';
    const WEEK_SECOND = '2';
    const WEEK_THIRD = '3';
    const WEEK_FOURTH = '4';
    const WEEK_FIFTH = '5';
    const WEEK_LAST = 'last';
}
