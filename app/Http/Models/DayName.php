<?php

namespace App\Http\Models;

class DayName
{
    const DAY_NAME_MONDAY = 'mon';
    const DAY_NAME_TUESDAY = 'tue';
    const DAY_NAME_WEDNESDAY = 'wed';
    const DAY_NAME_THURSDAY = 'thu';
    const DAY_NAME_FRIDAY = 'fri';
    const DAY_NAME_SATURDAY = 'sat';
    const DAY_NAME_SUNDAY = 'sun';

    public static function weekendDays(): array
    {
        return [self::DAY_NAME_SUNDAY, self::DAY_NAME_SATURDAY];
    }
}
