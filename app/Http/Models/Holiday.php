<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Holiday
 * @package App\Http\Models
 *
 * @property int $id
 * @property string $name
 * @property string $firstDate
 * @property string $lastDate
 * @property string $weekNumber
 * @property string $dayName
 */

class Holiday extends Model
{
    public $timestamps = false;
    protected $table = 'holiday';
    protected $guarded = ['id'];
}
