<?php

namespace App\Http\Helpers;

use App\Http\Models\Holiday;

class HolidayHelper
{
    public static function getCountDaysBetweenDates(Holiday $holiday, $presetYear): int
    {
        return (int)(new \DateTime(date('Y-m-d', self::createTimestamp(
            self::getMonthNumber($holiday->firstDate),
            self::getDayOfMonth($holiday->firstDate),
            $presetYear
        ))))
                ->diff(new \DateTime(date('Y-m-d', self::createTimestamp(
                    self::getMonthNumber($holiday->lastDate),
                    self::getDayOfMonth($holiday->lastDate),
                    $presetYear
                ))))
                ->format('%a') + 1;
    }

    public static function generateDateHoliday($name, $month, $day, $year): array
    {
        return [
            'month-day' => date('Y-m-d', self::createTimestamp($month, $day, $year)),
            'name' => $name,
        ];
    }

    public static function getWeekOfMonth($month, $day, $year): float
    {
        return ceil(date('j', self::createTimestamp($month, $day, $year)) / 7);
    }

    public static function getDayName($month, $day, $year): string
    {
        return strtolower(date('D', self::createTimestamp($month, $day, $year)));
    }

    public static function getYear($date): int
    {
        return (int)date('Y', strtotime($date));
    }

    public static function getMonthNumber($date): int
    {
        return (int)date('n', strtotime($date));
    }

    public static function getDaysCount($month, $day, $year): int
    {
        return (int)date('t', self::createTimestamp($month, $day, $year));
    }

    public static function getDayOfMonth($date): int
    {
        return (int)date('j', strtotime($date));
    }

    private static function createTimestamp($month, $day, $year): int
    {
        return mktime(0, 0, 0, $month, $day, $year);
    }
}
