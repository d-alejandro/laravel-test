<?php

if (!$date = old('date')) {
    if (session()->has('date')) {
        $date = session()->get('date');
    }
}
?>
@extends('layouts.app')

@section('content')

    <form method="POST" action="{{ route('holiday') }}">
        @csrf

        <div class="form-group">
            <label for="date">
                @if (session()->has('messages'))
                    @if($messages = session()->get('messages'))
                        @foreach($messages as $value)
                            It’s "{{ $value['name'] }}" on that date!<br>
                        @endforeach
                    @else
                        No holidays on that date!
                    @endif
                @endif
            </label>
            <input id="date" type="text" class="form-control" name="date" value="{{ $date }}"
                   aria-describedby="dateHelp" autocomplete="off" style="width: 220px;"
                   placeholder="Date Format: 31.01.2000" required autofocus>
            @if ($errors->has('date'))
                <small id="dateHelp" class="form-text text-muted" style="color: red !important;">
                    {{ $errors->first('date') }}
                </small>
            @endif
            @if (session()->has('error'))
                <p style="color: red;">{{ session()->get('error') }}</p>
            @endif
        </div>

        <button type="submit" class="btn btn-primary" style="width: 220px;">Send</button>

    </form>

@endsection