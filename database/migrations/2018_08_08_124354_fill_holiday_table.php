<?php

use Illuminate\Database\Migrations\Migration;
use App\Http\Models\HolidaysList;
use App\Http\Models\Holiday;

class FillHolidayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (HolidaysList::getList() as $holiday) {
            Holiday::create(
                [
                    'name' => $holiday->name,
                    'firstDate' => $holiday->firstDate,
                    'lastDate' => $holiday->lastDate,
                    'weekNumber' => $holiday->weekNumber,
                    'dayName' => $holiday->dayName
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Holiday::truncate();
    }
}
